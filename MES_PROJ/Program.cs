﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using MES.Model;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;

namespace MES_PROJ
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if(args != null)
            {
                Console.WriteLine("Hello MES!\n");
                for (int i = 0; i < args.Length; ++i)
                {
                    if (args[i].Substring(args[i].LastIndexOf(".")+1) == "json" && File.Exists(args[i]))
                    {
                        ThreadPool.QueueUserWorkItem(new WaitCallback(Computing), args[i]);
                    }
                }
                Console.ReadLine();
            }
        }

        private static readonly object ConsoleWriterLock = new object();
        private static void Computing(Object state)
        {
            string filename = state as string;
            Console.WriteLine("ThreadID: " + Thread.CurrentThread.ManagedThreadId +
                "\n" + "Reading: " + filename.Substring(filename.LastIndexOf("\\")+1));
            string output = "";
            int color = (int)ConsoleColor.White;
            try
            {
                double[,] matrix = new Grid(Simplex.BuildNodes(BasicSimplex.ReadJson(filename))).LinearEquations();
                LinearEquationSolver.Solve(matrix);
                output = JsonConvert.SerializeObject(LinearEquationSolver.OnlyResults(ref matrix));
                color = (int)ConsoleColor.DarkGreen;
            }
            catch (Exception ex)
            {
                output = JsonConvert.SerializeObject(ex);
                color = (int)ConsoleColor.Red;
            }
            finally
            {
                lock (ConsoleWriterLock)
                {
                    Console.ForegroundColor = (ConsoleColor)color;
                    using (StreamWriter writer = new StreamWriter(filename.Insert(filename.Length - 5, "_out")))
                    {
                        Console.WriteLine("ThreadID: " + Thread.CurrentThread.ManagedThreadId +
                            "\n" + "Writing: " + filename.Insert(filename.Length - 5, "_out").Substring(filename.LastIndexOf("\\") + 1));
                        writer.Write(output);
                    }
                    Console.ResetColor();
                }
            }
        }
    }
}
