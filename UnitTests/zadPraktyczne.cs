﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using MES.Model;

namespace UnitTests
{
    [TestClass]
    public class zadPraktyczne
    {
        [TestMethod]
        public void Demo1()
        {
            List<BasicSimplex> bs = new List<BasicSimplex>();
            Assert.AreEqual(bs.Count, 0);
            List<Dictionary<string, double>> d1 = new List<Dictionary<string, double>>(){
                new Dictionary<string, double>() {
                    {"q", -150}
                },
                new Dictionary<string, double>()
            };
            List<Dictionary<string, double>> d2 = new List<Dictionary<string, double>>(){
                new Dictionary<string, double>(),
                new Dictionary<string, double>() {
                    {"a", 10},
                    {"t", 40}
                }
            };
            bs.Add(new BasicSimplex(75, 1, 3.75, 
                                    new int[] {0, 1}, 
                                    d1));
            bs.Add(new BasicSimplex(75, 1, 3.75, 
                                    new int[] {1, 2}, 
                                    d2));

            Assert.AreEqual(bs.Count, 2);
            Assert.AreEqual(bs[1].BC[1].ContainsKey("a"), true);

            // create list of simplexes based on BasicSimplexes
            List<Simplex> s = Simplex.BuildNodes(bs);

            // validate values
            Assert.AreEqual(s[0].H[0, 0].Value, 20);
            Assert.AreEqual(s[0].H[0, 1].Value, -20);
            Assert.AreEqual(s[0].H[1, 0].Value, -20);
            Assert.AreEqual(s[0].H[1, 1].Value, 20);

            Assert.AreEqual(s[1].H[0, 0].Value, 20);
            Assert.AreEqual(s[1].H[0, 1].Value, -20);
            Assert.AreEqual(s[1].H[1, 0].Value, -20);
            Assert.AreEqual(s[1].H[1, 1].Value, 30);

            Grid g = new Grid(s);
            double[,] G = g.GenerateG();
            
            Assert.AreEqual(G.GetLength(0), 3);
            Assert.AreEqual(G.GetLength(1), 3);

            Assert.AreEqual(G[0,0], 20);
            Assert.AreEqual(G[0, 1], -20);
            Assert.AreEqual(G[0, 2], 0);
            Assert.AreEqual(G[1, 0], -20);
            Assert.AreEqual(G[1, 1], 40);
            Assert.AreEqual(G[1, 2], -20);
            Assert.AreEqual(G[2, 0], 0);
            Assert.AreEqual(G[2, 1], -20);
            Assert.AreEqual(G[2, 2], 30);

            double[] P = g.GenerateP();
            Assert.AreEqual(P.Length, 3);
            Assert.AreEqual(P[0], 150);
            Assert.AreEqual(P[1], 0);
            Assert.AreEqual(P[2], 400);
        }
    }
}
