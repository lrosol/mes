﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MES.Model;
using Newtonsoft.Json;


namespace UnitTests
{
    [TestClass]
    public class BasicSimplexTest
    {
        [TestMethod]
        public void SerializeJson()
        {
            Point<int, double> p = new Point<int, double>(new int[] { 1, 2 }, 5);

            BasicSimplex bs = new BasicSimplex(
                1,
                2,
                3,
                new int[] { 1, 2 },
                new List<Dictionary<string, double>>()

            );
            BasicSimplex bt = new BasicSimplex(
                3,
                2,
                1,
                new int[] { 2, 1 },
                new List<Dictionary<string, double>>()
            );

            List<BasicSimplex> list = new List<BasicSimplex>();
            list.Add(bs);
            list.Add(bt);

            BasicSimplex.WriteJson(list, "test.json");
            List<BasicSimplex> test_list = BasicSimplex.ReadJson("test.json");
            Assert.AreEqual(2, test_list.Count);
            Assert.AreEqual(2, test_list[1].NOP[0]);
        }
        [TestMethod]
        public void DeserilizeJson()
        {
            List<BasicSimplex> bs = BasicSimplex.ReadJson("grid.json");
            Assert.AreEqual(bs.Count, 2);
            BasicSimplex bs1 = bs[0];
            BasicSimplex bs2 = bs[1];
            Assert.AreEqual(bs1.BC.Count, 2);
            Assert.AreEqual(bs2.BC.Count, 2);

            Assert.AreEqual(bs1.BC[0].ContainsKey("q"), true);
            Assert.AreEqual(bs2.BC[1].ContainsKey("q"), false);
            Assert.AreEqual(bs2.BC[1].ContainsKey("a"), true);
            Assert.AreEqual(bs2.BC[1].ContainsKey("t"), true);

            Assert.AreEqual(bs1.K, 75);
            Assert.AreEqual(bs1.S, 1);
            Assert.AreEqual(bs1.L, 3.75);
            double[,] matrix = new Grid(Simplex.BuildNodes(bs)).LinearEquations();
            LinearEquationSolver.Solve(matrix);
            Console.WriteLine(JsonConvert.SerializeObject(LinearEquationSolver.OnlyResults(ref matrix)));
        }
    }

    [TestClass]
    public class PracticeTests
    {
        private List<BasicSimplex> bs;
        public PracticeTests()
        {
            bs = BasicSimplex.ReadJson("shield.json");
        }
        [TestMethod]
        public void LocalH()
        {
            Assert.AreEqual(3, bs.Count);

            Simplex s0 = new Simplex(bs[0]);
            Assert.AreEqual(s0.H[0, 0].Value, 10);
            Assert.AreEqual(s0.H[0, 1].Value, -10);
            Assert.AreEqual(s0.H[1, 0].Value, -10);
            Assert.AreEqual(s0.H[1, 1].Value, 10);

            Simplex s1 = new Simplex(bs[1]);
            Assert.AreEqual(s1.H[0, 0].Value, 1745);
            Assert.AreEqual(s1.H[0, 1].Value, -1745);
            Assert.AreEqual(s1.H[1, 0].Value, -1745);
            Assert.AreEqual(s1.H[1, 1].Value, 1745);

            Simplex s2 = new Simplex(bs[2]);
            Assert.AreEqual(1.75, s2.H[0, 0].Value, 0.000001);
            Assert.AreEqual(-1.75, s2.H[0, 1].Value, 0.000001);
            double a = s2.H[0, 1].Value;
            double b = -1.75;
            Assert.AreEqual(a, b, 0.000001);
            Assert.AreEqual(s2.H[1, 0].Value, -1.75, 0.0000001);
            Assert.AreEqual(s2.H[1, 1].Value, 7.75, 0.0000001);
        }

        [TestMethod]
        public void LocalP()
        {
            Assert.AreEqual(3, bs.Count);

            Simplex s0 = new Simplex(bs[0]);
            Assert.AreEqual(200, s0.P[0]);
            Assert.AreEqual(0, s0.P[1]);

            Simplex s1 = new Simplex(bs[1]);
            Assert.AreEqual(0, s1.P[0]);
            Assert.AreEqual(0, s1.P[1]);

            Simplex s2 = new Simplex(bs[2]);
            Assert.AreEqual(0, s2.P[0]);
            Assert.AreEqual(120, s2.P[1]);
        }

        [TestMethod]
        public void GlobalH()
        {
            Grid grid = new Grid(Simplex.BuildNodes(bs));
            double[,] g = grid.GenerateG();
            double t = 0.000001; // tolerance

            Assert.AreEqual(10, g[0, 0], t);
            Assert.AreEqual(-10, g[0, 1], t);
            Assert.AreEqual(0, g[0, 2], t);
            Assert.AreEqual(0, g[0, 3], t);
            Assert.AreEqual(-10, g[1, 0], t);
            Assert.AreEqual(1755, g[1, 1], t);
            Assert.AreEqual(-1745, g[1, 2], t);
            Assert.AreEqual(0, g[1, 3], t);
            Assert.AreEqual(0, g[2, 0], t);
            Assert.AreEqual(-1745, g[2, 1], t);
            Assert.AreEqual(1746.75, g[2, 2], t);
            Assert.AreEqual(-1.75, g[2, 3], t);
            Assert.AreEqual(0, g[3, 0], t);
            Assert.AreEqual(0, g[3, 1], t);
            Assert.AreEqual(-1.75, g[3, 2], t);
            Assert.AreEqual((1.75 + 6), g[3, 3], t);
        }

        [TestMethod]
        public void GlobalP()
        {
            Grid grid = new Grid(Simplex.BuildNodes(bs));
            double t = 0.000001; // tolerance
            double[] p = grid.GenerateP();

            Assert.AreEqual(4, p.Length);
            Assert.AreEqual(200, p[0], t);
            Assert.AreEqual(0, p[1], t);
            Assert.AreEqual(0, p[2], t);
            Assert.AreEqual(120, p[3], t);

        }
    }

    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Point<int, int> a = new Point<int, int>(2);
            Assert.AreEqual(a.getDimension(), a.Coordinates.Length, "Size of array has unexpected value");
        }
        [TestMethod]
        public void TestMethod2()
        {
            int val = 4;
            Point<double, double> a = new Point<double, double>(val);
            Assert.AreEqual(val, a.Coordinates.Length, "Size of array has unexpected value");
            Assert.AreEqual(a.getDimension(), val, "Size of array has unexpected value");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethod3()
        {
            Point<int, double> a = new Point<int, double>(10);
            a.getCoordiante(14);
        }

        [TestMethod]
        public void Creating_H_Matrix()
        {
            int[] arr = new int[3] { 5, 7, 8 };
            double[] p = new double[2] { 1, 0 };
            List<Point<int, double>> sm = new List<Point<int, double>>();
            List<Dictionary<string, double>> dic = new List<Dictionary<string, double>>();
            Simplex i = new Simplex(1, 2, 3, arr, dic);

            ArrayList a = new ArrayList();

            foreach (Point<int, double> x in i.H)
            {
                a.Add(x.getCoordiante(0));
                a.Add(x.getCoordiante(1));
            }
            int[] c = (int[])a.ToArray(typeof(int));
          
            int[] b = new int[] { 5, 5, 5, 7, 5, 8, 7, 5, 7, 7, 7, 8, 8, 5, 8, 7, 8, 8 };
            ArrayList d = new ArrayList(b);
            Assert.AreEqual(a.Count, d.Count, "Arrays have diffrents sizes");
            int arr_size = a.Count;
            for(int j=0; j<arr_size; ++j)
            {
                Assert.AreEqual(a[j], d[j], "Arrays have diffrent the value");
            }
        }

        [TestMethod]
        public void Creating_H_Matrix2()
        {
            int[] nop = new int[] { 2, 3 };
            double[] p = new double[] { 1, 0 };
            List<Point<int, double>> sm = new List<Point<int, double>>();
            List<Dictionary<string, double>> d = new List<Dictionary<string, double>>();
            Simplex s = new Simplex(1, 1, 1, nop, d);
            Assert.AreEqual(s.H[0, 0].getCoordiante((int)Dim.x), 2);
            Assert.AreEqual(s.H[0, 0].getCoordiante((int)Dim.y), 2);
            Assert.AreEqual(s.H[0, 0].Value, 1);

            Assert.AreEqual(s.H[0, 1].getCoordiante((int)Dim.x), 2);
            Assert.AreEqual(s.H[0, 1].getCoordiante((int)Dim.y), 3);
            Assert.AreEqual(s.H[0, 1].Value, -1);

            Assert.AreEqual(s.H[1, 0].getCoordiante((int)Dim.x), 3);
            Assert.AreEqual(s.H[1, 0].getCoordiante((int)Dim.y), 2);
            Assert.AreEqual(s.H[1, 0].Value, -1);

            Assert.AreEqual(s.H[1, 1].getCoordiante((int)Dim.x), 3);
            Assert.AreEqual(s.H[1, 1].getCoordiante((int)Dim.y), 3);
            Assert.AreEqual(s.H[1, 1].Value, 1);
        }

        [TestMethod]
        public void Creating_H_Matrix3()
        {
            int[] nop = new int[] {1,2,3};
            double[] p = new double[2] { 1, 0 };
            List<Point<int, double>> sm = new List<Point<int, double>>();
            List<Dictionary<string, double>> d = new List<Dictionary<string, double>>();
            Simplex s = new Simplex(3,4,2, nop, d);

            Assert.AreEqual(s.H[0, 0].getCoordiante((int)Dim.x), 1);
            Assert.AreEqual(s.H[0, 0].getCoordiante((int)Dim.y), 1);
            Assert.AreEqual(s.H[0, 0].Value, 6);

            Assert.AreEqual(s.H[0, 1].getCoordiante((int)Dim.x), 1);
            Assert.AreEqual(s.H[0, 1].getCoordiante((int)Dim.y), 2);
            Assert.AreEqual(s.H[0, 1].Value, -6);

            Assert.AreEqual(s.H[0, 2].getCoordiante((int)Dim.x), 1);
            Assert.AreEqual(s.H[0, 2].getCoordiante((int)Dim.y), 3);
            Assert.AreEqual(s.H[0, 2].Value, 6);

            Assert.AreEqual(s.H[1, 0].getCoordiante((int)Dim.x), 2);
            Assert.AreEqual(s.H[1, 0].getCoordiante((int)Dim.y), 1);
            Assert.AreEqual(s.H[1, 0].Value, -6);

            Assert.AreEqual(s.H[1, 1].getCoordiante((int)Dim.x), 2);
            Assert.AreEqual(s.H[1, 1].getCoordiante((int)Dim.y), 2);
            Assert.AreEqual(s.H[1, 1].Value, 6);

            Assert.AreEqual(s.H[1, 2].getCoordiante((int)Dim.x), 2);
            Assert.AreEqual(s.H[1, 2].getCoordiante((int)Dim.y), 3);
            Assert.AreEqual(s.H[1, 2].Value, 6);
        }

        [TestMethod]
        public void TestLinearEquationSolving1()
        {
            double[,] m = new double[,] { { 20, -20, 0, 150 }, { -20, 40, -20, 0 }, { 0, -20, 30, 400 } };
            bool b = LinearEquationSolver.Solve(m);
            Assert.AreEqual(b, true);
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 4; ++j)
                {
                    System.Console.WriteLine(m[i, j]);
                }
            }
            Assert.AreEqual(m[0, 3], 70);
            Assert.AreEqual(m[1, 3], 62.5);
            Assert.AreEqual(m[2, 3], 55);
        }

        [TestMethod]
        public void TestLinearEquationSolving2()
        {
            List<BasicSimplex> bs = BasicSimplex.ReadJson("grid.json");
            List<Simplex> s = Simplex.BuildNodes(bs);
            Grid g = new Grid(s);
            double[,] matrix = g.LinearEquations();
            LinearEquationSolver.Solve(matrix);

            bool b = LinearEquationSolver.Solve(matrix);
            Assert.AreEqual(b, true);
            double[] res = LinearEquationSolver.OnlyResults(ref matrix);
            Assert.AreEqual(res[0], 70);
            Assert.AreEqual(res[1], 62.5);
            Assert.AreEqual(res[2], 55);
            for (int i = 0; i < res.Length; ++i)
                Console.WriteLine(res[i]);
        }
    }
}
