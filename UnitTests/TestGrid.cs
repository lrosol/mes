﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MES.Model;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace UnitTests
{
    [TestClass]
    public class TestGrid
    {
        [TestMethod]
        public void TestCountNodes()
        {
            Grid g = new Grid();
            Assert.AreEqual(g.Count, 0);

            Simplex s1 = new Simplex(0,0,0, new int[] { 0, 1 } , new List<Dictionary<string, double>>());
            g.Add(s1);
            Assert.AreEqual(g.Count, 2);

            g.Add(new Simplex(0, 0, 0, new int[] { 0, 1 }, new List<Dictionary<string, double>>()));
            Assert.AreEqual(g.Count, 2);

            g.Add(new Simplex(0, 0, 0, new int[] { 1, 2 }, new List<Dictionary<string, double>>()));
            Assert.AreEqual(g.Count, 3);

            g.Add(new Simplex(0, 0, 0, new int[] { 10, 6 }, new List<Dictionary<string, double>>()));
            Assert.AreEqual(g.Count, 5);
        }

        [TestMethod]
        public void TestGenerateG()
        {
            // create list of simplexes based on BasicSimplexes
            List<Simplex> s = Simplex.BuildNodes(BasicSimplex.ReadJson("grid.json"));

            // validate values
            Assert.AreEqual(s[0].H[0, 0].Value, 20);
            Assert.AreEqual(s[0].H[0, 1].Value, -20);
            Assert.AreEqual(s[0].H[1, 0].Value, -20);
            Assert.AreEqual(s[0].H[1, 1].Value, 20);

            Assert.AreEqual(s[1].H[0, 0].Value, 20);
            Assert.AreEqual(s[1].H[0, 1].Value, -20);
            Assert.AreEqual(s[1].H[1, 0].Value, -20);
            Assert.AreEqual(s[1].H[1, 1].Value, 30);

            Grid grid = new Grid(s);

            double[,] G = grid.GenerateG();

            Assert.AreEqual(G.GetLength(0), 3);
            Assert.AreEqual(G.GetLength(1), 3);

            Assert.AreEqual(G[0, 0], 20);
            Assert.AreEqual(G[0, 1], -20);
            Assert.AreEqual(G[0, 2], 0);
            Assert.AreEqual(G[1, 0], -20);
            Assert.AreEqual(G[1, 1], 40);
            Assert.AreEqual(G[1, 2], -20);
            Assert.AreEqual(G[2, 0], 0);
            Assert.AreEqual(G[2, 1], -20);
            Assert.AreEqual(G[2, 2], 30);
        }

        [TestMethod]
        public void TestLinearEquaitons()
        {
            // create list of simplexes based on BasicSimplexes
            List<Simplex> s = Simplex.BuildNodes(BasicSimplex.ReadJson("grid.json"));

            Grid g = new Grid(s);
            double[,] m = g.LinearEquations();
            Assert.AreEqual(m.GetLength(0), m.GetLength(1) - 1);

            Assert.AreEqual(m[0, 0], 20);
            Assert.AreEqual(m[0, 1], -20);
            Assert.AreEqual(m[0, 2], 0);
            Assert.AreEqual(m[0, 3], 150);
            Assert.AreEqual(m[1, 0], -20);
            Assert.AreEqual(m[1, 1], 40);
            Assert.AreEqual(m[1, 2], -20);
            Assert.AreEqual(m[1, 3], 0);
            Assert.AreEqual(m[2, 0], 0);
            Assert.AreEqual(m[2, 1], -20);
            Assert.AreEqual(m[2, 2], 30);
            Assert.AreEqual(m[2, 3], 400);
        }

        [TestMethod]
        public void WatchData()
        {
            List<Simplex> s = Simplex.BuildNodes(BasicSimplex.ReadJson("input.json"));

            // show all H matrixes
            for (int i = 0; i < s.Count; ++i)
            {
                Console.WriteLine("Simplex[" + i + "]");
                for (int j = 0; j < s[i].H.GetLength(0); ++j)
                {
                    for (int h = 0; h < s[i].H.GetLength(0); ++h)
                    {
                        Console.Write(s[i].H[j, h].Value + ", ");
                    }
                    Console.WriteLine("\n");
                }
            }

            Grid grid = new Grid(s);
            double[,] G = grid.GenerateG();

            // show global matrix
            Console.WriteLine("Global");

            for (int i = 0; i < G.GetLength(0); ++i)
            {
                for (int j = 0; j < G.GetLength(0); ++j)
                {
                    Console.Write(G[i, j] + ", ");
                }
                Console.WriteLine("\n");
            }

            double[] P = grid.GenerateP();
            Console.WriteLine("P");
            for (int j = 0; j < P.Length; ++j)
            {
                Console.Write(P[j] + ", ");
                Console.WriteLine("\n");
            }
            
        }

        [TestMethod]
        public void genJson()
        {
            int[] arr = new int[3] { 5, 7, 8 };
            double[] p = new double[2] { 1, 0 };
            List<Point<int, double>> sm = new List<Point<int, double>>();
            List<Dictionary<string, double>> d = new List<Dictionary<string, double>>()
            {
                new Dictionary<string, double>(),
                new Dictionary<string, double>()
            };
            BasicSimplex i = new BasicSimplex(1, 2, 3, arr, d);
     
            Console.WriteLine(JsonConvert.SerializeObject(i));
       
        }
    }
}
