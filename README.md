# README #

### TO DO ###

[IM-1]

- Automatic computing numbers of points and set as public read only attribute (ec. dim) [Done]
- Improve generate P vector (currently vector P is computing from static values) [Done]
- Change SM type of attribute in BasicSimplex to HashMap (q=>3, S=> 5, t_ => 10) [Reject]

[IM-2]

- Organize code [Done]
- Add container for Simplexes. Container should contain method able to computing nodes, generate  G and P matrix [Done]

[IM-3]

- Support multi-threading [Done]

[IM-4]

- wektor P powinien być liczony przez program. należy wprowadzić zmienne BC (BoundaryCondition) dla każdego BasicSimplexu
- do programu wprowadzamy także globalne dane opisujące stan parametrów (\alpha , t_oo, itp.) z których będą liczone warunki brzegowe  
- Dodać kilka funkcji brzegowych implementujących ten sam interfejs przez który będzie odbywać się komunikacja w obliczeniach, przy wprowadzaniu wartości do wektora Ps

[

    {
        "K":75,
        "S":1,
        "L":3.75,
        "NOP":[0,1],
        "SM":[],
        //"bc":[2,0] // powinien określać że został zastosowany drugi warunek brzegowy
                     // na pierwszym nopie
        //"bc":[0,1] // określa że należy uwzględnić pierwszy warunek brzegowy dla
                     // drugiego nopa
        //"bc":[0,0] // powinien określać że w tym wypadku nie występują warunki brzegowe
    },
    {
        "K":75,
        "S":1.0,
        "L":3.75,
        "NOP":[1,7],
        "SM":[
            {
                "Coordinates":[1,1],
                "Value":10
            }
        ],
        "P":[0,400]
    },
    {

    }
]
