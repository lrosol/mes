﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Model
{
    public enum Dim
    {
        x = 0,
        y = 1,
        z = 2
    }
}
