﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace MES.Model
{
    public class BasicSimplex
    {
        [JsonConstructor]
        public BasicSimplex(double k, double s, double l, int[] nop, List<Dictionary<string, double>> bc)
        {
            K = k;
            S = s;
            L = l;
            NOP = nop;
            if (bc.Count == nop.Length)
            {
                BC = bc;
            }
            else
            {
                BC = new List<Dictionary<string, double>>();
                for (int i = 0; i < nop.Length; ++i)
                {
                    BC.Add(new Dictionary<string, double>());
                }
            }
        }

        public static List<BasicSimplex> ReadJson(string source = BasicSimplex.DefaultJsonSource)
        {
            using (StreamReader r = new StreamReader(source))
            {
                string str = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<BasicSimplex>>(str);
            }
        }
        public static void WriteJson(List<BasicSimplex> bs, string output = BasicSimplex.DefaultJsonOutput)
        {
            using (StreamWriter w = new StreamWriter(output))
            {
                string str = JsonConvert.SerializeObject(bs);
                w.Write(str);
            }
        }

        public double K
        {
            get { return _K; }
            set { _K = value; }
        }

        public double S
        {
            get { return _S; }
            set { _S = value; }
        }

        public double L
        {
            get { return _L; }
            set { _L = value; }
        }

        public int[] NOP
        {
            get { return _NOP; }
            set { _NOP = value; }
        }

        public List<Dictionary<string, double>> BC
        {
            get { return _BC; }
            set { _BC = value; }
        }

        private const string DefaultJsonSource = "input.json";
        private const string DefaultJsonOutput = "output.json";

        private double _K;                      // 
        private double _S;                      // Surface
        private double _L;                      // Length
        private int[] _NOP;                     // Number Of Points
        private List<Dictionary<string, double>> _BC; // boundary Conditions
    }  
}
