﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Model
{
    public class Grid
    {
        public Grid()
        {
            Nodes = new List<Simplex>();
        }
        public Grid(List<Simplex> s)
        {
            Nodes = s;
        }

        public void Add(Simplex sm)
        {
            Nodes.Add(sm);
        }

        public int Count
        {
            get { return CountNodes(); }
        }

        public double[,] GenerateG()
        {
            int dim = CountNodes();


            double[,] G = new double[dim, dim];
            foreach (Simplex s in Nodes)
            {
                for (int i = 0; i < s.pointsAmount; ++i)
                {
                    for (int j = 0; j < s.pointsAmount; ++j)
                    {
                        Point<int, double> p = s.H[i, j];
                        int x = p.getCoordiante((int)Dim.x);
                        int y = p.getCoordiante((int)Dim.y);
                        try
                        { 
                            G[x,y] += p.Value;
                        }
                        catch (Exception ex)
                        {
                            System.Console.WriteLine();
                            if(x >= G.GetLength(0) || y >= G.GetLength(1))
                            {
                                throw new Exception("Can't generate global matrix. Please make sure that number of points is equal to the higher index points. Index out of range for G[ " + x + ", " + y + " ]");
                            }
                            throw ex;
                        }
                    }
                }
            }
            return G;
        }

        public double[] GenerateP()
        {
            int dim = CountNodes();

            double[] p = new double[dim];
            foreach(Simplex s in Nodes)
            {
                for(int i=0; i<s.P.Length; ++i)
                {
                    try
                    {
                        p[s.NOP[i]] += s.P[i];
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }

            return p;
        }

        public double[,] LinearEquations()
        {
            double[,] g = GenerateG();
            double[] p = GenerateP();
            int n = g.GetLength(0);
            int m = g.GetLength(1) + 1;
            double[,] result = new double[n,m];

            for(int i=0; i<n; ++i)
            {
                for(int j=0; j<n; ++j)
                {
                    result[i, j] = g[i, j];
                }
                result[i, m - 1] = p[i];
            }
            return result;
        }
        private List<Simplex> Nodes
        {
            get { return _Nodes; }
            set { _Nodes = value; }
        }

        private int CountNodes()
        {
            List<int> uniqueNodes = new List<int>();

            foreach(Simplex s in Nodes)
            {
                for(int i =0; i<s.NOP.Length; i++)
                {
                    int val = s.NOP[i];
                    if(!uniqueNodes.Exists(x=>x == val))
                    {
                        uniqueNodes.Add(val);
                    }
                }
            }

            return uniqueNodes.Count;
        }

        private List<Simplex> _Nodes;
    }
}
