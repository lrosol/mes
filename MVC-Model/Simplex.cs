﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Newtonsoft.Json;

namespace MES.Model
{
    public class Simplex : BasicSimplex
    {
        public Simplex(double k, double s, double l, int[] nop, List<Dictionary<string, double>> bc):
            base(k, s, l, nop, bc)
        {
            _nopLength = NOP.Length;

            // reserves memory resources
            _NM = new int[2, 2] { { 1, -1 }, { -1, 1 } };
            P = new double[] { 0, 0 };
            initializeLocalMatrix(out _H, 2);

            // fill matrix _H values from NOP
            updateLocalMatrix();
        }

        public Simplex(BasicSimplex bs)
            : this(bs.K, bs.S, bs.L, bs.NOP, bs.BC) {}

        public static List<Simplex> BuildNodes(List<BasicSimplex> bs)
        {
            List<Simplex> s = new List<Simplex>();
            foreach (BasicSimplex b in bs)
            {
                s.Add(new Simplex(b));
            }
            return s;
        }

        public static double[] GenerateP(List<Simplex> _s, int _d = 0)
        {
            int dim;
            if (_d == 0)
            {
                //dim = Simplex.LastNOP;
                throw new Exception("Nowt implemented");
            }
            else
            {
                dim = _d;
            }
            double[] p = new double[dim];
            p[0] = _s[0].P[0];
            p[dim - 1] = _s[_s.Count - 1].P[1];
                
            return p;
        }

        public int pointsAmount
        {
            get { return _nopLength; }
        }

        public Point<int, double>[,] H
        {
            get {return _H; }
        }

        public double getValue(int x, int y)
        {
            try
            {
                return H[x, y].Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string toString()
        {
            return JsonConvert.SerializeObject(this);
        }
        private void initializeLocalMatrix(out Point<int, double>[,] matrix, int dim)
        {
            // initialize empty matrix _H
            matrix = new Point<int, double>[pointsAmount, pointsAmount];
            for (int i = 0; i < pointsAmount; ++i)
            {
                for (int j = 0; j < pointsAmount; ++j)
                {
                    matrix[i, j] = new Point<int, double>(dim);
                }
            }
        }
        private void updateLocalMatrix()
        {
            for (int i=0; i<pointsAmount; ++i)
            {
                for(int j=0; j<pointsAmount; ++j)
                {
                    _H[i,j].setCoordinate((int)Dim.x, NOP[i]);
                    _H[i,j].setCoordinate((int)Dim.y, NOP[j]);
                    _H[i, j].Value = ((K * S) / L);
                    if (i < 2 && j < 2)
                    {
                        _H[i, j].Value *= _NM[i, j];
                    }
                }
            }
            for (int i = 0; i < pointsAmount; ++i)
            {
                if(BC[i].ContainsKey("a"))
                {
                    _H[i, i].Value += BC[i]["a"] * S;
                     if(BC[i].ContainsKey("t"))
                     {
                         P[i] = -1 * -1 * BC[i]["a"] * BC[i]["t"] * S;
                     }
                }
                else if(BC[i].ContainsKey("q"))
                {
                    P[i] = -1 * BC[i]["q"] * S;
                }
            }
           
        }

        public double[] P
        {
            get { return _P; }
            set { _P = value; }
        }

        private double[] _P; 
        private int _nopLength; // points count
        private Point<int, double>[,] _H; // local matix of values
        private int[,] _NM;  // constant results of shape function
    }
}
